//
//  MovieService.swift
//  Top Movies
//
//  Created by Orhun Dündar on 19.12.2021.
//

import Foundation
import Alamofire
import ObjectMapper

class MovieService: WebService {
    
    public static let apiKey: String = "43a1e305566aa173466e66c3957cb268"
    public static let imageBaseStr: String = "https://image.tmdb.org/t/p/"
    public static let tmdbApiBaseUrl: String = "https://api.themoviedb.org/3/"
    
    static func getPopularMovies(page:Int, success: @escaping ([MovieListItem]) -> Void, error: @escaping () -> Void){
        
        let url = tmdbApiBaseUrl + "tv/popular?api_key=\(apiKey)&page=\(page)"
        
        AlamofireManager!.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil )
            .responseJSON(completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    let response = value as! NSDictionary
                    
                    if let result:GetPopularMoviesResultModel = Mapper<GetPopularMoviesResultModel>().map(JSONObject: response) {
                        
                        guard let array = result.results else {
                            error()
                            return
                        }
                        success(array)
                    }
                    else {
                        error()
                    }
                    break
                    
                case .failure(_):
                    error()
                    break
                 }
            })
        
    }
    
    
    static func getMovieDetail(id:Int, success: @escaping (GetMovieDetailResultModel) -> Void, error: @escaping () -> Void){
        
        let url = tmdbApiBaseUrl + "tv/\(id)?api_key=\(apiKey)"
        
        AlamofireManager!.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil )
            .responseJSON(completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    let response = value as! NSDictionary
                    
                    if let result:GetMovieDetailResultModel = Mapper<GetMovieDetailResultModel>().map(JSONObject: response) {
                        success(result)
                    }
                    else {
                        error()
                    }
                    break
                    
                case .failure(_):
                    error()
                    break
                 }
            })
        
    }
    
}
