//
//  WebService.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import Alamofire
import ObjectMapper
import SwiftyJSON
import AlamofireObjectMapper

class WebService {
    
    //Check internet connection
    public static var isReachable:Bool {
        get { return NetworkReachabilityManager()?.isReachable ?? false }
    }
    
    public static var AlamofireManager: SessionManager? = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        return alamoFireManager
    }()
    
    internal class func sendGetRequest(url:String, parameters:Parameters? ,success: @escaping (NSDictionary) -> Void ,fail: @escaping (String) -> Void) {
        if !isReachable {
            // No internet connection
            fail("No internet connection")
        }else {
            
            let safeURL = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            AlamofireManager!.request(safeURL, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil )
                .responseJSON(completionHandler: { response in
                    
                    switch(response.result) {
                    case .success(let value):
                        let response = value as! NSDictionary
                        success(response)
                        break
                        
                    case .failure(let error):
                        fail("Unexpected Error: " + error.localizedDescription)
                        break
                     }
                })
        }
    }
    
}
