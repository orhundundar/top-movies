//
//  LoadingView.swift
//  Top Movies
//
//  Created by Orhun Dündar on 20.12.2021.
//


import UIKit

class LoadingView: UIView {
    
    static let shared = LoadingView()
    
    private var isLoadingViewShowing:Bool = false
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        //for debug validation
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.40)
        
        let whiteBackgraund = UIView(frame: CGRect(x: self.frame.midX-60, y: self.frame.midY-60, width: 120, height: 120))
        whiteBackgraund.backgroundColor = UIColor(white: 1, alpha: 1)
        whiteBackgraund.layer.cornerRadius = 60
        
        let logoImageView = UIImageView(image: UIImage(named: "loadingImage"))
        logoImageView.frame = CGRect(x: 17, y: 13, width: 86, height: 86)
        whiteBackgraund.addSubview(logoImageView)
        
        
        let path = UIBezierPath(roundedRect: whiteBackgraund.bounds.insetBy(dx: 2, dy: 2), byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight], cornerRadii: CGSize(width: 60, height: 60))

        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: whiteBackgraund.frame.size)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [UIColor.black.cgColor, UIColor.white.cgColor]

        let shape = CAShapeLayer()
        shape.lineWidth = 4
        shape.path = path.cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        let animation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = [UIColor.black.cgColor, UIColor.white.cgColor]
        animation.toValue = [UIColor.white.cgColor, UIColor.black.cgColor]
        animation.duration = 0.5
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.isRemovedOnCompletion = false

        // add the animation to the gradient
        gradient.add(animation, forKey: nil)

        whiteBackgraund.layer.insertSublayer(gradient, at: 0)
        
        self.addSubview(whiteBackgraund)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    public func startLoadingView(vc:UIViewController){
        if !isLoadingViewShowing {
            vc.view.addSubview(self)
            isLoadingViewShowing = true
        }
    }
    
    public func startLoadingView(){
        if !isLoadingViewShowing {
            
            if let window = UIApplication.shared.keyWindow {
                if let rootViewController = window.rootViewController {
                    rootViewController.view.addSubview(self)
                }
            }
            isLoadingViewShowing = true
        }
    }
    
    public func stopLoadingView(){
        isLoadingViewShowing=false
        self.removeFromSuperview()
    }
    
}
