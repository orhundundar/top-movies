//
//  MovieListItem.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import ObjectMapper

class MovieListItem : Mappable {
    var poster_path:String?
    var popularity:Int!
    var id:Int!
    var overview:String!
    var genre_ids:[Int]!
    var original_language:String!
    var vote_average:Float!
    var name:String!
    private(set)var posterImageURLMedium: String?
    private(set)var posterImageURLHigh: String?
    
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        poster_path <- map["poster_path"]
        popularity <- map["popularity"]
        id <- map["id"]
        overview <- map["overview"]
        genre_ids <- map["genre_ids"]
        original_language <- map["original_language"]
        vote_average <- map["vote_average"]
        name <- map["name"]
        
        if let posterImagePath = poster_path {
            self.posterImageURLMedium = (MovieService.imageBaseStr + "w500" + posterImagePath)
            self.posterImageURLHigh = (MovieService.imageBaseStr + "original" + posterImagePath)
        }
    }
}
