//
//  PopularMoviesResultModel.swift
//  Top Movies
//
//  Created by Orhun Dündar on 19.12.2021.
//

import ObjectMapper

class GetPopularMoviesResultModel : Mappable {
    var page:Double?
    var results:[MovieListItem]!
    var total_pages:Double!
    var total_results:Double!
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        page <- map["page"]
        results <- map["results"]
        total_pages <- map["total_pages"]
        total_results <- map["total_results"]
        
    }
}
