//
//  GetMovieDetailResultModel.swift
//  Top Movies
//
//  Created by Orhun Dündar on 20.12.2021.
//


import ObjectMapper

class GetMovieDetailResultModel : Mappable {
    var backdrop_path:String?
    var genres:[MovieGenre]!
    var id:Double!
    var name:String!
    var overview:String!
    var poster_path:String?
    var vote_average:Float!
    private(set)var posterImageURL: String?
    
    var genresString: String {
        return self.genres.map({$0.name}).joined(separator: ",")
    }
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        backdrop_path <- map["backdrop_path"]
        genres <- map["genres"]
        id <- map["id"]
        name <- map["name"]
        overview <- map["overview"]
        poster_path <- map["poster_path"]
        vote_average <- map["vote_average"]
        
        if let posterImagePath = poster_path {
            self.posterImageURL = (MovieService.imageBaseStr + "w500" + posterImagePath)
        }
    }
}

class MovieGenre : Mappable {
    var name:String!
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        
    }
}
