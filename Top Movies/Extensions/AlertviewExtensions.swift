//
//  AlertviewExtensions.swift
//  Top Movies
//
//  Created by Orhun Dündar on 20.12.2021.
//

import UIKit

extension UIAlertController {
    
    static func showAlert(vc: UIViewController, title: String?, message: String?, buttons: [UIAlertAction]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        buttons.forEach { button in
            alert.addAction(button)
        }
        
        vc.present(alert, animated: true, completion: nil)
    }
}
