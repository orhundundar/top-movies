//
//  MovieDetailViewModel.swift
//  Top Movies
//
//  Created by Orhun Dündar on 20.12.2021.
//

import Foundation
import RxCocoa
import RxSwift


class MovieDetailViewModel {
    
    var movieID:Int!
    var movie = BehaviorRelay<GetMovieDetailResultModel?>(value: nil)
    var error = BehaviorRelay<String?>(value: nil)
    
    init(movieID:Int) {
        self.movieID = movieID
        self.getMovieDetail()
    }
    
    private func getMovieDetail(){
        MovieService.getMovieDetail(id: self.movieID) { result in
            self.movie.accept(result)
        } error: {
            self.error.accept("TODO: Movie Detail couldn't loaded.")
        }
    }
    
}
