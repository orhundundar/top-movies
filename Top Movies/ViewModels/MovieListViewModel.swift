//
//  MovieListViewModel.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import Foundation
import RxCocoa
import RxSwift

class MovieListViewModel {
    
    var movies = BehaviorRelay<[MovieListItem]>(value: [])
    var error = BehaviorRelay<String?>(value: nil)
    private var currentPage : Int = 1
    
    private let disposeBag = DisposeBag()
    
    init() {
        self.getPopularMovies()
    }
    
    func getPopularMovies(){
        
        MovieService.getPopularMovies(page: currentPage) { movieList in
            self.movies.accept(self.movies.value + movieList)
            self.currentPage += 1
        } error: {
            self.error.accept("TODO: Movie List Couldn't loaded.")
        }
        
    }
    
    
}
