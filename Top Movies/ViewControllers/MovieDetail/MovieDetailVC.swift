//
//  MovieDetailVC.swift
//  Top Movies
//
//  Created by Orhun Dündar on 20.12.2021.
//

import UIKit
import RxCocoa
import RxSwift

class MovieDetailVC: UIViewController {
    
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    @IBOutlet weak var movieGenreLabel: UILabel!
    @IBOutlet weak var movieVoteLabel: UILabel!
    
    var movieID:Int!
    var viewModel:MovieDetailViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = MovieDetailViewModel(movieID: movieID)
        
        //For Movie Detail datas
        viewModel.movie.asDriver().drive(onNext: {[unowned self] (_) in
            
            moviePosterImageView.sd_setImage(with: URL(string: viewModel.movie.value?.posterImageURL ?? ""), placeholderImage: UIImage(named: "placeholder"))
            movieNameLabel.text = viewModel.movie.value?.name
            movieOverviewLabel.text = viewModel.movie.value?.overview
            movieGenreLabel.text = viewModel.movie.value?.genresString
            let avarage:Float = viewModel.movie.value?.vote_average ?? 0
            movieVoteLabel.text = "\(String(format: "%.1f", avarage))/10"
            
            LoadingView.shared.stopLoadingView()
            
        }).disposed(by: disposeBag)
        
        //For Error
        viewModel.error.asDriver().drive(onNext: {[unowned self] (error) in
            if error != nil {
                UIAlertController.showAlert(vc: self, title: "Error", message: viewModel.error.value, buttons: [UIAlertAction(title: "OK", style: .default, handler: { _ in
                    
                })])
                LoadingView.shared.stopLoadingView()
            }
        }).disposed(by: disposeBag)
    }
    
}
