//
//  MovieListVC.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import UIKit
import RxCocoa
import RxSwift

class MovieListVC: UIViewController {
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    var viewModel: MovieListViewModel!
    let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingView.shared.startLoadingView()
        initViews()
        
        viewModel = MovieListViewModel()
        
        // For Movie List
        viewModel.movies.asDriver().drive(onNext: {[unowned self] (_) in
            self.moviesTableView.reloadData()
            LoadingView.shared.stopLoadingView()
        }).disposed(by: disposeBag)
        
        //For Error
        viewModel.error.asDriver().drive(onNext: {[unowned self] (error) in
            if error != nil {
                UIAlertController.showAlert(vc: self, title: "Error", message: viewModel.error.value, buttons: [UIAlertAction(title: "OK", style: .default, handler: { _ in
                    
                })])
                LoadingView.shared.stopLoadingView()
            }
        }).disposed(by: disposeBag)
    }
    
    private func initViews(){
        self.title = "Top Movies"
        
        //Set tableview
        moviesTableView.delegate = self
        moviesTableView.dataSource = self
        moviesTableView.register(MoviesTableViewCell.nib, forCellReuseIdentifier: "MoviesTableViewCell")
        moviesTableView.separatorInset = .zero
        
    }
    
}


extension MovieListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.movies.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesTableViewCell", for: indexPath) as! MoviesTableViewCell
        cell.setCell(movie: viewModel.movies.value[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        LoadingView.shared.startLoadingView()
        
        let movieDetail = MovieDetailVC(nibName: "MovieDetailVC", bundle: nil)
        movieDetail.movieID = viewModel.movies.value[indexPath.row].id
        self.navigationController?.pushViewController(movieDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = viewModel.movies.value.count - 1
        if indexPath.row == lastItem {
            LoadingView.shared.startLoadingView()
            viewModel.getPopularMovies()
        }
    }
}
