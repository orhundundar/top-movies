//
//  MoviesTableViewCell.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import UIKit
import SDWebImage

class MoviesTableViewCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieRateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        borderView.layer.cornerRadius = 8
        borderView.layer.masksToBounds = true
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor.systemGray6.cgColor
        
        movieTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        movieRateLabel.textColor = UIColor.darkText
        
        movieRateLabel.font = UIFont.boldSystemFont(ofSize: 15)
        movieRateLabel.textColor = UIColor.darkGray
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override var bounds: CGRect {
        didSet {
          contentView.frame = bounds
        }
    }
    
    override func didMoveToSuperview() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        
    }
    
    func setCell(movie:MovieListItem){
        moviePosterImageView.sd_setImage(with: URL(string: movie.posterImageURLMedium ?? ""), placeholderImage: UIImage(named: "placeholder"))
        movieTitleLabel.text = movie.name
        movieRateLabel.text = "\(String(format: "%.1f", movie.vote_average))/10"
        
    }
    
}
