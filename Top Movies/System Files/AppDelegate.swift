//
//  AppDelegate.swift
//  Top Movies
//
//  Created by Orhun Dündar on 16.12.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Navigationbar
        UINavigationBar.appearance().barTintColor = .systemGray6
        UINavigationBar.appearance().tintColor = .darkGray
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        UINavigationBar.appearance().isTranslucent = true
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let viewController = MovieListVC(nibName: "MovieListVC", bundle: nil)
        let navigationController = UINavigationController.init(rootViewController: viewController)
        self.window?.rootViewController = navigationController
        
        self.window?.makeKeyAndVisible()
        return true
    }
    
    
}

